setwd("/users/mis/rolezac1/forecasts")  ##### Set working directory
# # ### common path for shared libraries
.libPaths('/apps/EnterpriseAnalytics/R_Projects/sharedLibrary')
library(zoo)
library(quadprog)
library(tseries)
library(sandwich)
library(strucchange)
library(foreign)
#library(Defaults)
library(xts)
library(TTR)
library(plyr)
library(MASS)
library(boot)
library(survey)
library(mitools)
library(corpcor)
library(relaimpo)
library(quantmod)
library(DMwR)
library(circular)
library(wle)
library(timeDate)
library(e1071)
library(forecast)
library(pracma)
library(caret)
# Uncomment the above if not called from external file

options(warn=-1)#suppress warnings, ARIMA function throws warnings in denied ARIMA models


###############################
#Change the next 3 lines only#
###############################
#setwd("C:/Users/zach.roley/Documents/Yield Management/ESPN example code")   ##### Set working directory
base_data<-read.csv("espn_sample_data.csv")
function_path <- "time_slot_prediction.R"

#Prepare prediction table
predictions <- as.data.frame(as.Date(with(base_data, paste(year, month, day,sep="-")), "%Y-%m-%d"))
colnames(predictions)[1] <- "Date"
last_day <- predictions$Date[nrow(predictions)]
next_ten <- as.data.frame(last_day + c(1:10))
colnames(next_ten)[1] <- "Date"
predictions<- rbind(predictions,next_ten)

#Prepare diagnositcs table
diagnostics <- data.frame(MAPE = numeric(),maxerror = numeric(),mape10day_avg = numeric(),mapeSD = numeric(),pred10day_avg = numeric())


timeslot<-0    #Global variable for the current timeslot
for(timeslot in 1:(ncol(base_data)-3)){

     #Prediction Table
     predict <-as.data.frame(source(function_path, echo=TRUE))  # Call prediction script
     predict$visible<-NULL
     colnames(predict)[1] <- paste("Timeslot",timeslot,"Demand") # Rename columns
     colnames(predict)[2] <- paste("Timeslot",timeslot,"Predicted")
     colnames(predict)[3] <- paste("Timeslot",timeslot,"Error")
     predictions<-cbind(predictions,predict)
     
     #Calculate Error
     MAPE <- sapply(predict[3],mean, na.rm = TRUE)
     maxerror<- max(predict[3],na.rm=TRUE)
     mape10day_avg<- sapply(tail(predict[3],n=20), mean, na.rm=TRUE)
     mapeSD<-sapply(predict[3], sd, na.rm=TRUE)
     pred_frame<- data.frame(predict[2])
     pred10day_avg<- sapply(tail(pred_frame, n=5), mean)
     
     #Final Diagnostic table
     diag<-as.data.frame(cbind(MAPE, maxerror, mape10day_avg,mapeSD,pred10day_avg)) ##final table for diagnostic check to select the ARIMA model##
     diagnostics <- rbind(diagnostics,diag)
}

#Write final tables to file
# write.csv(predictions,file = "network_predictions.csv",row.names=FALSE)
# write.csv(diagnostics,file = "model_diagnostics.csv")
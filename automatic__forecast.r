#library(xlsx) ### Can be used if preferred
library(XLConnect) ### Used for auto-updating the excel sheet of forecasts
library(forecast)
library(plyr)

### Set Working Directory
setwd("C:/Users/victor.geary/Documents")

### Extracting Raw Data
raw_Data<-read.delim("C:/Users/victor.geary/Desktop/Project Code & Resources/Data/ra_data_7_27.txt", header=TRUE, sep="|")

### Clean up raw data
colnames(raw_Data) <- c("create_date", "model", "disco_type", "return_date")
raw_Data$create_date <- as.Date(raw_Data$create_date, format = "%Y-%m-%d")
raw_Data$return_date <- as.Date(raw_Data$return_date, format = "%Y-%m-%d")
raw_Data$week_day <- weekdays(as.Date(raw_Data$return_date))

## Create daily forecast subset
daily_subset <- raw_Data[!(raw_Data$week_day == "Friday" | raw_Data$week_day == "Saturday" | raw_Data$week_day == "Sunday"),]
daily_subset <- subset(daily_subset, daily_subset$return_date > "1753-01-01")

### Create weekly RA based forecast subset
weekly_subset <- raw_Data
weekly_subset$ra_age <- (weekly_subset$return_date - weekly_subset$create_date)
weekly_subset$ra_age <- as.numeric(weekly_subset$ra_age)
weekly_subset$returned <- ifelse(weekly_subset$ra_age < 0,0,1)

### --- Daily Model --- ###

### Future exploration and implementation of other data splits may be necissary for improved accuracy
### and futher information for the business/manufacturing team. Priority of exploration/implementation
### would be as follows: 1) Split by disconnect type (did this in earlier builds, worth implementing)
### 2) Split by processing plant (El Paso vs Spartanburg) 3) Split by UPS parcel and IHS bulk shipment
### Once this further exploration is completed, the final models can be created and expanded to all 
### reciever types.

### Split data by reciever type and clean up data
h3Data<-subset(daily_subset, daily_subset$model == 'HOPPER3')
h3Returns<-data.frame(table(h3Data$return_date))
rm(h3Data)
colnames(h3Returns) <- c("Date", "Returns")
h3Returns$week_no <- strftime(h3Returns$Date,format="%W")
h3Returns$week_day <- weekdays(as.Date(h3Returns$Date))
h3Returns$day <- strftime(h3Returns$Date,format="%d")
h3Returns$year <- strftime(h3Returns$Date,format="%Y")
h3Returns <- h3Returns[(nrow(h3Returns)-179):nrow(h3Returns),]

h2Data<-subset(daily_subset, daily_subset$model == 'HPR2000')
h2Returns<-data.frame(table(h2Data$return_date))
rm(h2Data)
colnames(h2Returns) <- c("Date", "Returns")
h2Returns$week_no <- strftime(h2Returns$Date,format="%W")
h2Returns$week_day <- weekdays(as.Date(h2Returns$Date))
h2Returns$day <- strftime(h2Returns$Date,format="%d")
h2Returns$year <- strftime(h2Returns$Date,format="%Y")
h2Returns <- h2Returns[(nrow(h2Returns)-179):nrow(h2Returns),]

hSData<-subset(daily_subset, daily_subset$model == 'HPRSLNG')
hSReturns<-data.frame(table(hSData$return_date))
rm(hSData)
colnames(hSReturns) <- c("Date", "Returns")
hSReturns$week_no <- strftime(hSReturns$Date,format="%W")
hSReturns$week_day <- weekdays(as.Date(hSReturns$Date))
hSReturns$day <- strftime(hSReturns$Date,format="%d")
hSReturns$year <- strftime(hSReturns$Date,format="%Y")
hSReturns <- hSReturns[(nrow(hSReturns)-179):nrow(hSReturns),]

j1Data<-subset(daily_subset, daily_subset$model == 'JOEY1.0')
j1Returns<-data.frame(table(j1Data$return_date))
rm(j1Data)
colnames(j1Returns) <- c("Date", "Returns")
j1Returns$week_no <- strftime(j1Returns$Date,format="%W")
j1Returns$week_day <- weekdays(as.Date(j1Returns$Date))
j1Returns$day <- strftime(j1Returns$Date,format="%d")
j1Returns$year <- strftime(j1Returns$Date,format="%Y")
j1Returns <- j1Returns[(nrow(j1Returns)-179):nrow(j1Returns),]

j2Data<-subset(daily_subset, daily_subset$model == 'JOEY2.0')
j2Returns<-data.frame(table(j2Data$return_date))
rm(j2Data)
colnames(j2Returns) <- c("Date", "Returns")
j2Returns$week_no <- strftime(j2Returns$Date,format="%W")
j2Returns$week_day <- weekdays(as.Date(j2Returns$Date))
j2Returns$day <- strftime(j2Returns$Date,format="%d")
j2Returns$year <- strftime(j2Returns$Date,format="%Y")
j2Returns <- j2Returns[(nrow(j2Returns)-179):nrow(j2Returns),]

sJData<-subset(daily_subset, daily_subset$model == 'SUPERJOEY')
sJReturns<-data.frame(table(sJData$return_date))
rm(sJData)
colnames(sJReturns) <- c("Date", "Returns")
sJReturns$week_no <- strftime(sJReturns$Date,format="%W")
sJReturns$week_day <- weekdays(as.Date(sJReturns$Date))
sJReturns$day <- strftime(sJReturns$Date,format="%d")
sJReturns$year <- strftime(sJReturns$Date,format="%Y")
sJReturns <- sJReturns[(nrow(sJReturns)-179):nrow(sJReturns),]

wJData<-subset(daily_subset, daily_subset$model == 'WIFIJOEY')
wJReturns<-data.frame(table(wJData$return_date))
rm(wJData)
colnames(wJReturns) <- c("Date", "Returns")
wJReturns$week_no <- strftime(wJReturns$Date,format="%W")
wJReturns$week_day <- weekdays(as.Date(wJReturns$Date))
wJReturns$day <- strftime(wJReturns$Date,format="%d")
wJReturns$year <- strftime(wJReturns$Date,format="%Y")
wJReturns <- wJReturns[(nrow(wJReturns)-179):nrow(wJReturns),]

### Add historical returns data to excel spreadsheet
master_data <- h3Returns[,1:2]
colnames(master_data) <- c("Date", "h3Returns")
master_data$h2Returns <- h2Returns$Returns
master_data$hSReturns <- hSReturns$Returns
master_data$j1Returns <- j1Returns$Returns
master_data$j2Returns <- j2Returns$Returns
master_data$sJReturns <- sJReturns$Returns
master_data$wJReturns <- wJReturns$Returns

wb <- loadWorkbook("Automated_Forecast_Sheet.xlsx")
writeWorksheet(object = wb, data = master_data, sheet = "Raw_Data", startCol = 10)
saveWorkbook(wb)

### Create Daily ARIMA Forecasts with values based off best performing training models
fit <- arima(h3Returns$Returns, order=c(1,1,2))
forecast <- as.data.frame(forecast(fit, 10))
forecast <- forecast[1]
colnames(forecast)[1] <- "h3_forecast"
master_forecast <- forecast

fit <- arima(h2Returns$Returns, order=c(4,1,4))
forecast <- as.data.frame(forecast(fit, 10))
forecast <- forecast[1]
colnames(forecast)[1] <- "h2_forecast"
master_forecast$h2_forecast <- forecast$h2_forecast

fit <- arima(hSReturns$Returns, order=c(4,0,5))
forecast <- as.data.frame(forecast(fit, 10))
forecast <- forecast[1]
colnames(forecast)[1] <- "hS_forecast"
master_forecast$hS_forecast <- forecast$hS_forecast

fit <- arima(j1Returns$Returns, order=c(1,1,3))
forecast <- as.data.frame(forecast(fit, 10))
forecast <- forecast[1]
colnames(forecast)[1] <- "j1_forecast"
master_forecast$j1_forecast <- forecast$j1_forecast

fit <- arima(j2Returns$Returns, order=c(5,0,3))
forecast <- as.data.frame(forecast(fit, 10))
forecast <- forecast[1]
colnames(forecast)[1] <- "j2_forecast"
master_forecast$j2_forecast <- forecast$j2_forecast

fit <- arima(sJReturns$Returns, order=c(3,1,2))
forecast <- as.data.frame(forecast(fit, 10))
forecast <- forecast[1]
colnames(forecast)[1] <- "sJ_forecast"
master_forecast$sJ_forecast <- forecast$sJ_forecast

fit <- arima(wJReturns$Returns, order=c(4,1,3))
forecast <- as.data.frame(forecast(fit, 10))
forecast <- forecast[1]
colnames(forecast)[1] <- "wJ_forecast"
master_forecast$wJ_forecast <- forecast$wJ_forecast

### Export data to excel sheet
wb <- loadWorkbook("Automated_Forecast_Sheet.xlsx")
writeWorksheet(object = wb, data = master_forecast, sheet = "Raw_Data")
saveWorkbook(wb)

### --- Weekly Model --- ###

### ARIMA models for RA creations forecasts may need to be retrained, have not validated accuracy as of now.
### Further exploration of forecasting return percentage instead of using an average may increase accuracy.
### Also need to validate the aggregate of all subset models error vs just the total returns model error. (split-total vs. total)

### Create weekly forecast for each reciever type
### Hopper 3
### Frame RA Creations
h3Ra <- subset(weekly_subset, weekly_subset$model == 'HOPPER3')
ra_creations <- data.frame(table(h3Ra$create_date))
colnames(ra_creations) <- c("Date", "RA_Creations")
ra_creations <- ra_creations[(nrow(ra_creations)-179):nrow(ra_creations),]

### Forecast 14 Days of RA Creations
ra_fit <- arima(ra_creations$RA_Creations, order=c(4,1,1))
ra_forecast <- as.data.frame(forecast(ra_fit, 14))
ra_forecast <- ra_forecast[1]
colnames(ra_forecast)[1] <- "RA_forecast"

### Calculate RA Age Distribution
age_subset <- subset(h3Ra, h3Ra$create_date < (max(h3Ra$create_date, na.rm = TRUE)-180))
age_subset <- subset(age_subset, age_subset$returned == 1)
age_dist <- data.frame(table(age_subset$ra_age))
age_dist$dist <- age_dist$Freq/nrow(age_subset)

### Calculate Return Percentage (don't forecast due to compounding error, use average of past 120 observations)
return_percent <- aggregate(returned ~ create_date, h3Ra, FUN = "mean")
return_percent <- return_percent[1:(nrow(return_percent)-180),]
rp <- mean(return_percent$returned)

### Forecast the Next Two Weeks of Returns
ra_returns_forecast <- as.data.frame(ra_creations$RA_Creations)
for(i in 1:14){
  ra_returns_forecast[(180+i),1] <- ra_forecast[i,1]
}

colnames(ra_returns_forecast) <- "RA_Creations"
ra_returns_forecast$returns <- 0

## Calculate Model Values
for(j in 1:(nrow(ra_returns_forecast))){
  for(k in 0:(nrow(age_dist)-1)){
    ra_returns_forecast[(j+k),2] <- ra_returns_forecast[(j+k),2] + (ra_returns_forecast[j,1]*age_dist[k+1,3]*rp)
  }
}

ra_returns_forecast<-ra_returns_forecast[181:194,]
master_wk_forecast <- as.data.frame(ra_returns_forecast) 
colnames(master_wk_forecast) <- c("h3_ra_creations","h3_wk_forecast")

### Hopper Sling

### Frame RA Creations
hSRa <- subset(weekly_subset, weekly_subset$model == 'HPRSLNG')
ra_creations <- data.frame(table(hSRa$create_date))
colnames(ra_creations) <- c("Date", "RA_Creations")
ra_creations <- ra_creations[(nrow(ra_creations)-179):nrow(ra_creations),]

### Forecast 14 Days of RA Creations
ra_fit <- arima(ra_creations$RA_Creations, order=c(3,1,1))
ra_forecast <- as.data.frame(forecast(ra_fit, 14))
ra_forecast <- ra_forecast[1]
colnames(ra_forecast)[1] <- "RA_forecast"

### Calculate RA Age Distribution
age_subset <- subset(hSRa, hSRa$create_date < (max(hSRa$create_date, na.rm = TRUE)-180))
age_subset <- subset(age_subset, age_subset$returned == 1)
age_dist <- data.frame(table(age_subset$ra_age))
age_dist$dist <- age_dist$Freq/nrow(age_subset)

### Calculate Return Percentage (don't forecast due to compounding error, use average of past 120 observations)
return_percent <- aggregate(returned ~ create_date, hSRa, FUN = "mean")
return_percent <- return_percent[1:(nrow(return_percent)-180),]
rp <- mean(return_percent$returned)

### Forecast the Next Two Weeks of Returns
ra_returns_forecast <- as.data.frame(ra_creations$RA_Creations)
for(i in 1:14){
  ra_returns_forecast[(180+i),1] <- ra_forecast[i,1]
}

colnames(ra_returns_forecast) <- "RA_Creations"
ra_returns_forecast$returns <- 0

## Calculate Model Values
for(j in 1:(nrow(ra_returns_forecast))){
  for(k in 0:(nrow(age_dist)-1)){
    ra_returns_forecast[(j+k),2] <- ra_returns_forecast[(j+k),2] + (ra_returns_forecast[j,1]*age_dist[k+1,3]*rp)
  }
}

ra_returns_forecast<-ra_returns_forecast[181:194,]
master_wk_forecast$hs_ra_creations <- ra_returns_forecast$RA_Creations
master_wk_forecast$hs_wk_forecast <- ra_returns_forecast$returns

### Hopper 2000

### Frame RA Creations
h2Ra <- subset(weekly_subset, weekly_subset$model == 'HPR2000')
ra_creations <- data.frame(table(h2Ra$create_date))
colnames(ra_creations) <- c("Date", "RA_Creations")
ra_creations <- ra_creations[(nrow(ra_creations)-179):nrow(ra_creations),]

### Forecast 14 Days of RA Creations
ra_fit <- arima(ra_creations$RA_Creations, order=c(3,1,1))
ra_forecast <- as.data.frame(forecast(ra_fit, 14))
ra_forecast <- ra_forecast[1]
colnames(ra_forecast)[1] <- "RA_forecast"

### Calculate RA Age Distribution
age_subset <- subset(h2Ra, h2Ra$create_date < (max(h2Ra$create_date, na.rm = TRUE)-180))
age_subset <- subset(age_subset, age_subset$returned == 1)
age_dist <- data.frame(table(age_subset$ra_age))
age_dist$dist <- age_dist$Freq/nrow(age_subset)

### Calculate Return Percentage (don't forecast due to compounding error, use average of past 120 observations)
return_percent <- aggregate(returned ~ create_date, h2Ra, FUN = "mean")
return_percent <- return_percent[1:(nrow(return_percent)-180),]
rp <- mean(return_percent$returned)

### Forecast the Next Two Weeks of Returns
ra_returns_forecast <- as.data.frame(ra_creations$RA_Creations)
for(i in 1:14){
  ra_returns_forecast[(180+i),1] <- ra_forecast[i,1]
}

colnames(ra_returns_forecast) <- "RA_Creations"
ra_returns_forecast$returns <- 0

## Calculate Model Values
for(j in 1:(nrow(ra_returns_forecast))){
  for(k in 0:(nrow(age_dist)-1)){
    ra_returns_forecast[(j+k),2] <- ra_returns_forecast[(j+k),2] + (ra_returns_forecast[j,1]*age_dist[k+1,3]*rp)
  }
}

ra_returns_forecast<-ra_returns_forecast[181:194,]
master_wk_forecast$h2_ra_creations <- ra_returns_forecast$RA_Creations
master_wk_forecast$h2_wk_forecast <- ra_returns_forecast$returns

### Joey 1.0

### Frame RA Creations
j1Ra <- subset(weekly_subset, weekly_subset$model == 'JOEY1.0')
ra_creations <- data.frame(table(j1Ra$create_date))
colnames(ra_creations) <- c("Date", "RA_Creations")
ra_creations <- ra_creations[(nrow(ra_creations)-179):nrow(ra_creations),]

### Forecast 14 Days of RA Creations
ra_fit <- arima(ra_creations$RA_Creations, order=c(4,1,1))
ra_forecast <- as.data.frame(forecast(ra_fit, 14))
ra_forecast <- ra_forecast[1]
colnames(ra_forecast)[1] <- "RA_forecast"

### Calculate RA Age Distribution
age_subset <- subset(j1Ra, j1Ra$create_date < (max(j1Ra$create_date, na.rm = TRUE)-180))
age_subset <- subset(age_subset, age_subset$returned == 1)
age_dist <- data.frame(table(age_subset$ra_age))
age_dist$dist <- age_dist$Freq/nrow(age_subset)

### Calculate Return Percentage (don't forecast due to compounding error, use average of past 120 observations)
return_percent <- aggregate(returned ~ create_date, j1Ra, FUN = "mean")
return_percent <- return_percent[1:(nrow(return_percent)-180),]
rp <- mean(return_percent$returned)

### Forecast the Next Two Weeks of Returns
ra_returns_forecast <- as.data.frame(ra_creations$RA_Creations)
for(i in 1:14){
  ra_returns_forecast[(180+i),1] <- ra_forecast[i,1]
}

colnames(ra_returns_forecast) <- "RA_Creations"
ra_returns_forecast$returns <- 0

## Calculate Model Values
for(j in 1:(nrow(ra_returns_forecast))){
  for(k in 0:(nrow(age_dist)-1)){
    ra_returns_forecast[(j+k),2] <- ra_returns_forecast[(j+k),2] + (ra_returns_forecast[j,1]*age_dist[k+1,3]*rp)
  }
}

ra_returns_forecast<-ra_returns_forecast[181:194,]
master_wk_forecast$j1_ra_creations <- ra_returns_forecast$RA_Creations
master_wk_forecast$j1_wk_forecast <- ra_returns_forecast$returns

### Joey 2.0

### Frame RA Creations
j2Ra <- subset(weekly_subset, weekly_subset$model == 'JOEY2.0')
ra_creations <- data.frame(table(j2Ra$create_date))
colnames(ra_creations) <- c("Date", "RA_Creations")
ra_creations <- ra_creations[(nrow(ra_creations)-179):nrow(ra_creations),]

### Forecast 14 Days of RA Creations
ra_fit <- arima(ra_creations$RA_Creations, order=c(4,1,1))
ra_forecast <- as.data.frame(forecast(ra_fit, 14))
ra_forecast <- ra_forecast[1]
colnames(ra_forecast)[1] <- "RA_forecast"

### Calculate RA Age Distribution
age_subset <- subset(j2Ra, j2Ra$create_date < (max(j2Ra$create_date, na.rm = TRUE)-180))
age_subset <- subset(age_subset, age_subset$returned == 1)
age_dist <- data.frame(table(age_subset$ra_age))
age_dist$dist <- age_dist$Freq/nrow(age_subset)

### Calculate Return Percentage (don't forecast due to compounding error, use average of past 120 observations)
return_percent <- aggregate(returned ~ create_date, j2Ra, FUN = "mean")
return_percent <- return_percent[1:(nrow(return_percent)-180),]
rp <- mean(return_percent$returned)

### Forecast the Next Two Weeks of Returns
ra_returns_forecast <- as.data.frame(ra_creations$RA_Creations)
for(i in 1:14){
  ra_returns_forecast[(180+i),1] <- ra_forecast[i,1]
}

colnames(ra_returns_forecast) <- "RA_Creations"
ra_returns_forecast$returns <- 0

## Calculate Model Values
for(j in 1:(nrow(ra_returns_forecast))){
  for(k in 0:(nrow(age_dist)-1)){
    ra_returns_forecast[(j+k),2] <- ra_returns_forecast[(j+k),2] + (ra_returns_forecast[j,1]*age_dist[k+1,3]*rp)
  }
}

ra_returns_forecast<-ra_returns_forecast[181:194,]
master_wk_forecast$j2_ra_creations <- ra_returns_forecast$RA_Creations
master_wk_forecast$j2_wk_forecast <- ra_returns_forecast$returns

### Super Joey

### Frame RA Creations
sJRa <- subset(weekly_subset, weekly_subset$model == 'SUPERJOEY')
ra_creations <- data.frame(table(sJRa$create_date))
colnames(ra_creations) <- c("Date", "RA_Creations")
ra_creations <- ra_creations[(nrow(ra_creations)-179):nrow(ra_creations),]

### Forecast 14 Days of RA Creations
ra_fit <- arima(ra_creations$RA_Creations, order=c(1,0,1))
ra_forecast <- as.data.frame(forecast(ra_fit, 14))
ra_forecast <- ra_forecast[1]
colnames(ra_forecast)[1] <- "RA_forecast"

### Calculate RA Age Distribution
age_subset <- subset(sJRa, sJRa$create_date < (max(sJRa$create_date, na.rm = TRUE)-180))
age_subset <- subset(age_subset, age_subset$returned == 1)
age_dist <- data.frame(table(age_subset$ra_age))
age_dist$dist <- age_dist$Freq/nrow(age_subset)

### Calculate Return Percentage (don't forecast due to compounding error, use average of past 120 observations)
return_percent <- aggregate(returned ~ create_date, sJRa, FUN = "mean")
return_percent <- return_percent[1:(nrow(return_percent)-180),]
rp <- mean(return_percent$returned)

### Forecast the Next Two Weeks of Returns
ra_returns_forecast <- as.data.frame(ra_creations$RA_Creations)
for(i in 1:14){
  ra_returns_forecast[(180+i),1] <- ra_forecast[i,1]
}

colnames(ra_returns_forecast) <- "RA_Creations"
ra_returns_forecast$returns <- 0

## Calculate Model Values
for(j in 1:(nrow(ra_returns_forecast))){
  for(k in 0:(nrow(age_dist)-1)){
    ra_returns_forecast[(j+k),2] <- ra_returns_forecast[(j+k),2] + (ra_returns_forecast[j,1]*age_dist[k+1,3]*rp)
  }
}

ra_returns_forecast<-ra_returns_forecast[181:194,]
master_wk_forecast$sJ_ra_creations <- ra_returns_forecast$RA_Creations
master_wk_forecast$sJ_wk_forecast <- ra_returns_forecast$returns

### Wifi Joey

### Frame RA Creations
wJRa <- subset(weekly_subset, weekly_subset$model == 'WIFIJOEY')
ra_creations <- data.frame(table(wJRa$create_date))
colnames(ra_creations) <- c("Date", "RA_Creations")
ra_creations <- ra_creations[(nrow(ra_creations)-179):nrow(ra_creations),]

### Forecast 14 Days of RA Creations
ra_fit <- arima(ra_creations$RA_Creations, order=c(3,1,4))
ra_forecast <- as.data.frame(forecast(ra_fit, 14))
ra_forecast <- ra_forecast[1]
colnames(ra_forecast)[1] <- "RA_forecast"

### Calculate RA Age Distribution
age_subset <- subset(wJRa, wJRa$create_date < (max(wJRa$create_date, na.rm = TRUE)-180))
age_subset <- subset(age_subset, age_subset$returned == 1)
age_dist <- data.frame(table(age_subset$ra_age))
age_dist$dist <- age_dist$Freq/nrow(age_subset)

### Calculate Return Percentage (don't forecast due to compounding error, use average of past 120 observations)
return_percent <- aggregate(returned ~ create_date, wJRa, FUN = "mean")
return_percent <- return_percent[1:(nrow(return_percent)-180),]
rp <- mean(return_percent$returned)

### Forecast the Next Two Weeks of Returns
ra_returns_forecast <- as.data.frame(ra_creations$RA_Creations)
for(i in 1:14){
  ra_returns_forecast[(180+i),1] <- ra_forecast[i,1]
}

colnames(ra_returns_forecast) <- "RA_Creations"
ra_returns_forecast$returns <- 0

### Calculate Model Values
for(j in 1:(nrow(ra_returns_forecast))){
  for(k in 0:(nrow(age_dist)-1)){
    ra_returns_forecast[(j+k),2] <- ra_returns_forecast[(j+k),2] + (ra_returns_forecast[j,1]*age_dist[k+1,3]*rp)
  }
}

ra_returns_forecast<-ra_returns_forecast[181:194,]
master_wk_forecast$wJ_ra_creations <- ra_returns_forecast$RA_Creations
master_wk_forecast$wJ_wk_forecast <- ra_returns_forecast$returns

### Total
### Frame RA Creations
ra_creations <- data.frame(table(weekly_subset$create_date))
colnames(ra_creations) <- c("Date", "RA_Creations")
ra_creations <- ra_creations[(nrow(ra_creations)-179):nrow(ra_creations),]

### Forecast 14 Days of RA Creations
ra_fit <- arima(ra_creations$RA_Creations, order=c(3,1,1))
ra_forecast <- as.data.frame(forecast(ra_fit, 14))
ra_forecast <- ra_forecast[1]
colnames(ra_forecast)[1] <- "RA_forecast"

### Calculate RA Age Distribution
age_subset <- subset(weekly_subset, weekly_subset$create_date < (max(weekly_subset$create_date, na.rm = TRUE)-180))
age_subset <- subset(age_subset, age_subset$returned == 1)
age_dist <- data.frame(table(age_subset$ra_age))
age_dist$dist <- age_dist$Freq/nrow(age_subset)

### Calculate Return Percentage (don't forecast due to compounding error, use average of past 120 observations)
return_percent <- aggregate(returned ~ create_date, weekly_subset, FUN = "mean")
return_percent <- return_percent[1:(nrow(return_percent)-180),]
rp <- mean(return_percent$returned)

### Forecast the Next Two Weeks of Returns
ra_returns_forecast <- as.data.frame(ra_creations$RA_Creations)
for(i in 1:14){
  ra_returns_forecast[(180+i),1] <- ra_forecast[i,1]
}

colnames(ra_returns_forecast) <- "RA_Creations"
ra_returns_forecast$returns <- 0

## Calculate Model Values
for(j in 1:(nrow(ra_returns_forecast))){
  for(k in 0:(nrow(age_dist)-1)){
    ra_returns_forecast[(j+k),2] <- ra_returns_forecast[(j+k),2] + (ra_returns_forecast[j,1]*age_dist[k+1,3]*rp)
  }
}

ra_returns_forecast<-ra_returns_forecast[181:194,]
master_wk_forecast$total_ra_creations <- ra_returns_forecast$RA_Creations
master_wk_forecast$total_wk_forecast <- ra_returns_forecast$returns


### Write weekly forecast to excel sheet
wb <- loadWorkbook("Automated_Forecast_Sheet.xlsx")
writeWorksheet(object = wb, data = master_wk_forecast, sheet = "Raw_Data", startCol = 19)
saveWorkbook(wb)
